# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=wayshot
pkgver=1.1.8
pkgrel=0
pkgdesc="A native screenshot tool for wlroots based compositors"
url="https://github.com/waycrate/wayshot"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # blocked by rust/cargo
license="BSD-2-Clause"
depends="wlroots"
makedepends="cargo"
source="https://github.com/waycrate/wayshot/archive/$pkgver/wayshot-$pkgver.tar.gz
	update-cargo-lock.patch
	"
options="!check"  # no tests provided

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

package() {
	install -D -m755 target/release/wayshot -t "$pkgdir"/usr/bin/
}

sha512sums="
7672a679da701fa0e208caf1d20181495a649aafc295c5559b657b3251ee1bde1565ce87ed7ed8531d2b75f22f18b7f9a7aaf5481182e179d3fc7a1e5652e087  wayshot-1.1.8.tar.gz
a1fd1eb1722e2ab15bf681c701402f91366839fae10fd91ead0d922784a1aa624e55fdf8db5219ce20b71979526b8604fe989c55d485058f2fbc59d715b5de3e  update-cargo-lock.patch
"
